<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sergeid');
            $table->string('title')->default(null)->nullable();
            $table->string('street');
            $table->string('street_number')->nullable();
            $table->string('zipcode');
            $table->string('city')->default("Augsburg");
            $table->string('district');
            $table->string('latitude');
            $table->string('longtitude');
            $table->string('comment')->default(null)->nullable();
            $table->boolean('needs_review');
            $table->boolean('is_published');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
