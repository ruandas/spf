<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //
    public function objects()
    {
        return $this->belongsToMany(Object::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
