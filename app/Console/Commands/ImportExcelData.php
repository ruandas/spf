<?php

namespace App\Console\Commands;

use App\Object;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports data from a provided Excel Sheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //make sure the file exists
        $exists = Storage::disk('public')->exists('spfdata.xls');
        #dd($exists);

        if($exists){
            //get filepath
            $filepath = Storage::disk('public')->url('spfdata.xls');

            Excel::load($filepath, function($reader){
                //the loop
                $reader->each(function($row){

                    //try to analyze address
                    $adresse = explode(', ', $row['adresse']);

                    for($i = 0;$i<count($adresse);$i++){
                        $adressetemp[] = explode(' ',$adresse[$i]);
                    }

                    $street = $adressetemp[0][0];
                    $street_number = $adressetemp[0][1];
                    $zipcode = $adressetemp[1][0];
                    $city = $adressetemp[1][1];

                    //verify authenticy of address
                    if(is_null($street)){
                        dd('error at ' . $row . ' -> $street is null!');
                    }
                    if(is_null($street_number)){
                        dd('error at ' . $row . ' -> $street_number is null!');
                    }
                    if(is_null($zipcode)){
                        dd('error at ' . $row . ' -> $zipcode is null!');
                    }
                    if(is_null($city)){
                        dd('error at ' . $row . ' -> $city is null!');
                    }

                    //object related stuff
                    $object['sergeid'] = $row['id'];
                    $object['title'] = $row['name'];
                    $object['street'] = $street;
                    $object['street_number'] = $street_number;
                    $object['zipcode'] = $zipcode;
                    $object['city'] = $city;
                    $object['latitude'] = $row['geogr.breite'];
                    $object['longtitude'] = $row['gegr.lange'];
                    $object['comment'] = $row['bemerkungen_und_notizen'];
                    #dd($object);

                    //barrier_free
                    $barrier_free = true;
                    if($row['barrierefreiheit'] == 'Nein'){
                        $barrier_free = false;
                    }

                    //spielgeräte
                    $equipment = explode('; ',$row['spielgerate']);
                    #dd($equipment);

                    foreach($equipment as $item){
                        $equipmenttemp = explode(' x',$item);
                        #dd($eqipmenttemp);

                        $equipmentname = '';
                        switch($equipmenttemp[0]){
                            case '01':
                                $equipmentname = 'Rutsche';
                                break;
                            case '02':
                                $equipmentname = 'Wippe';
                                break;
                            case '03':
                                $equipmentname = 'Seilbahn';
                                break;
                            case '04':
                                $equipmentname = 'Wasserspiel';
                                break;
                            case '05':
                                $equipmentname = 'Sandkasten';
                                break;
                            case '06':
                                $equipmentname = 'Schaukel';
                                break;
                            case '07':
                                $equipmentname = 'Karusell';
                                break;
                            case '08':
                                $equipmentname = 'Klettergerüst';
                                break;
                            case '09':
                                $equipmentname = 'Spielhaus';
                                break;
                            default:
                                $equipmentname = $equipmenttemp[0];
                                break;
                        }
                        #dd($equipmentname);

                        $equipmentcount = $equipmenttemp[1];
                        #dd($equipmentcount);
                    }

                    //umgebungsausstattung
                    $areaequipment = explode('; ',$row['umgebungsausstattung']);

                    foreach($areaequipment as $item){

                        $areaequipmentname = '';
                        switch($item){
                            case '01':
                                $areaequipmentname = 'WC';
                                break;
                            case '02':
                                $areaequipmentname = 'Gastronomie';
                                break;
                            case '03':
                                $areaequipmentname = 'Notruf';
                                break;
                            case '04':
                                $areaequipmentname = 'Kiosk';
                                break;
                            case '05':
                                $areaequipmentname = 'Einkaufen';
                                break;
                            default:
                                $areaequipmentname = $item;
                                break;
                        }
                    }

                    //category
                    $categories = explode('; ',$row['objektart']);

                    foreach($categories as $item){

                        $category = '';
                        switch($item){
                            case '01':
                                $category = 'Spielplatz';
                                break;
                            case '02':
                                $category = 'Indoorspielplatz';
                                break;
                            case '03':
                                $category = 'Spielwiese';
                                break;
                            case '04':
                                $category = 'Bolzplatz';
                                break;
                            case '05':
                                $category = 'Mehrgenerationenspielplatz';
                                break;
                            case '06':
                                $category = 'Skateranlage';
                                break;
                            case '07':
                                $category = 'Wasserspielplatz';
                                break;
                            case '08':
                                $category = 'Spielraum';
                                break;
                            case '09':
                                $category = 'Park';
                                break;
                            case '10':
                                $category = 'Waldspielplatz';
                                break;
                            case '11':
                                $category = 'Hochseilgarten';
                                break;
                            case '12':
                                $category = 'Abenteuerspielplatz';
                                break;
                            default:
                                $category = $item;
                                break;
                        }
                    }

                    $database_object = new Object;


                    dd($row);





                });

            })->get();


        }
        else{
            dd('there appears to be no file');
        }

    }
}
