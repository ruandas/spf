<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
    //
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }
}
