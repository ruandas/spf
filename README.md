# Spielplatzführer Augsburg

Projekt um die Augsburger Spielplätze übersichtlich zusammenzufassen und nach Auswahlkriterien durchsuchen zu können.

## You need this:
* [Laravel](https://laravel.com/) - The web framework used


## Versioning

We use [Bitbucket](http://bitbucket.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/ruandas/spf). 

## Authors

* **Fred**
* **Daniel**
* **Rafael**

Thanks Serge for an outstanding database to begin with.

